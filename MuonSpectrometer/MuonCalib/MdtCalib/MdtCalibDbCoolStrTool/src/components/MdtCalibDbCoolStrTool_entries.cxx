#include "MdtCalibDbCoolStrTool/MdtCalibDbCoolStrTool.h"
#include "MdtCalibDbCoolStrTool/MdtCalibDbAlg.h"
#include "MdtCalibSvc/MdtCalibrationRegionSvc.h"

using namespace MuonCalib;

DECLARE_COMPONENT( MdtCalibDbCoolStrTool )
DECLARE_COMPONENT( MdtCalibDbAlg )
